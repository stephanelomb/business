import { Component, OnInit } from '@angular/core';
import { StorageService } from '../../services/storage/storage.service';
import { Administration } from '../../models/administration.model';


@Component({
  selector: 'app-personal-page',
  templateUrl: './personal-page.component.html',
  styleUrls: ['./personal-page.component.css']
})
export class PersonalPageComponent implements OnInit {

  model: Administration;
  constructor(private storageService: StorageService) { }

  ngOnInit() {
    this.model = this.storageService.getAdministration();
  }

}
