import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Administration } from '../../models/administration.model';
import { StorageService } from '../../services/storage/storage.service';

@Component({
  selector: 'app-administration',
  templateUrl: './administration.component.html',
  styleUrls: ['./administration.component.css']
})
export class AdministrationComponent implements OnInit {

  step: number;
  model: Administration;
  constructor(private storageService: StorageService, private router: Router) { }

  ngOnInit() {
    this.step = 1;
    this.model = this.storageService.getAdministration();
    this.model = this.model ? this.model : new Administration();
    console.log(this.model);
  }

  nextStep = (newStep: number) => {
    this.step = newStep;
  }

  save = () => {
    this.storageService.saveAdministration(this.model);
    this.router.navigate(['/personalPage']);
  }

}
