import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Legal } from '../../../models/legal.model';

@Component({
  selector: 'app-legal',
  templateUrl: './legal.component.html',
  styleUrls: ['./legal.component.css']
})
export class LegalComponent implements OnInit {

  submitTried = true;

  @Output()
  previous: EventEmitter<any>;

  @Output()
  next: EventEmitter<any>;

  @Input()
  data: Legal;

  constructor() {
    this.previous = new EventEmitter<any>();
    this.next = new EventEmitter<any>();
    this.submitTried = false;
  }

  ngOnInit() {
  }

  trySubmit() {
    this.submitTried = true;
  }

  goPrevious() {
    this.previous.emit();
  }

  submit() {
    this.next.emit();
  }

}
