import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { SectorList } from '../../../models/sectorList.model';
import { Basic } from '../../../models/basic.model';

@Component({
  selector: 'app-basic',
  templateUrl: './basic.component.html',
  styleUrls: ['./basic.component.css']
})
export class BasicComponent implements OnInit {

  @Output()
  next: EventEmitter<any>;

  @Input()
  data: Basic;

  sectors: SectorList[];
  subSectors: SectorList[];
  submitTried: boolean;

  constructor() {
    this.next = new EventEmitter<any>();
    this.submitTried = false;
  }

  ngOnInit() {
    this.fillSector();
    this.fillSubSector();
  }

  submit() {
    this.next.emit();
  }

  trySubmit() {
    this.submitTried = true;
  }

  sectorChanged() {
    this.data.subSector = undefined;
  }

  fillSector() {
    this.sectors = [
      {
        id: '1',
        label: 'Public'
      },
      {
        id: '2',
        label: 'Private'
      },
      {
        id: '3',
        label: 'Other'
      },
    ];
  }
  fillSubSector() {
    this.subSectors = [
      {
        id: '1',
        label: 'Education',
        idRef: '1'
      },
      {
        id: '2',
        label: 'Medical',
        idRef: '1'
      },
      {
        id: '3',
        label: 'Social',
        idRef: '1'
      },
      {
        id: '4',
        label: 'Other',
        idRef: '1'
      },
      {
        id: '5',
        label: 'Bank',
        idRef: '2'
      },
      {
        id: '6',
        label: 'Construction',
        idRef: '2'
      },
      {
        id: '7',
        label: 'IT',
        idRef: '2'
      },
      {
        id: '8',
        label: 'Trade',
        idRef: '2'
      },
      {
        id: '9',
        label: 'Transport',
        idRef: '2'
      },
      {
        id: '10',
        label: 'Other',
        idRef: '2'
      },
      {
        id: '11',
        label: 'Other',
        idRef: '3'
      }
    ];
  }

}
