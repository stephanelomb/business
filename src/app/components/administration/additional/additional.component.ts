import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Additional } from '../../../models/additional.model';


@Component({
  selector: 'app-additional',
  templateUrl: './additional.component.html',
  styleUrls: ['./additional.component.css']
})
export class AdditionalComponent implements OnInit {

  @Output()
  next: EventEmitter<any>;

  @Output()
  previous: EventEmitter<any>;

  @Input()
  data: Additional;

  submitTried: boolean;

  constructor() {
    this.next = new EventEmitter<any>();
    this.previous = new EventEmitter<any>();
    this.submitTried = false;
  }

  ngOnInit() {
    this.data.initWeek([
      'ADMINISTRATION.FORM.WEEK.MO',
      'ADMINISTRATION.FORM.WEEK.TU',
      'ADMINISTRATION.FORM.WEEK.WE',
      'ADMINISTRATION.FORM.WEEK.TH',
      'ADMINISTRATION.FORM.WEEK.FR',
      'ADMINISTRATION.FORM.WEEK.SA',
      'ADMINISTRATION.FORM.WEEK.SU'
    ]);
  }

  submit() {
    this.next.emit();
  }
  goPrevious() {
    this.previous.emit();
  }
  trySubmit() {
    this.submitTried = true;
  }

}
