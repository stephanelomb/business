import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Media } from '../../../models/media.model';

@Component({
  selector: 'app-media',
  templateUrl: './media.component.html',
  styleUrls: ['./media.component.css']
})
export class MediaComponent implements OnInit {

  @Output()
  next: EventEmitter<any>;

  @Output()
  previous: EventEmitter<any>;

  @Input()
  data: Media;

  private logoReader: FileReader;
  private avatarReader: FileReader;
  private galeryReader: FileReader;
  private galeryEditReader: FileReader;
  private tempEdit: number;

  constructor() {
    const temp = this.tempEdit;
    this.next = new EventEmitter<any>();
    this.previous = new EventEmitter<any>();
    this.logoReader = new FileReader();
    this.avatarReader = new FileReader();
    this.galeryReader = new FileReader();
    this.galeryEditReader = new FileReader();
    const that = this;
    this.logoReader.addEventListener('load', function () {
      that.data.logo = that.logoReader.result;
    }, false);
    this.avatarReader.addEventListener('load', function () {
      that.data.avatar = that.avatarReader.result;
    }, false);
    this.galeryReader.addEventListener('load', function () {
      that.data.addToGalery(that.galeryReader.result);
    }, false);
    this.galeryEditReader.addEventListener('load', function () {
      that.data.setGalery(that.galeryReader.result, temp);
    }, false);

  }

  ngOnInit() {
  }

  submit() {
    this.next.emit();
  }

  setLogo(files: File[]) {
    if (files.length > 0) {
      this.logoReader.readAsDataURL(files[0]);
    }
  }
  setAvatar(files: File[]) {
    if (files.length > 0) {
      this.avatarReader.readAsDataURL(files[0]);
    }
  }

  addPictureToGalery(files: File[]) {
    if (files.length > 0) {
      this.galeryReader.readAsDataURL(files[0]);
    }
  }

  setGalery(files: File[], tempEdit: number) {
    this.tempEdit = tempEdit;
    this.galeryEditReader.readAsDataURL(files[0]);
  }

  goPrevious() {
    this.previous.emit();
  }

}
