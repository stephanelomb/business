import { Basic } from '../models/basic.model';
import { Additional } from './additional.model';
import { Legal } from './legal.model';
import { Media } from './media.model';

export class Administration {
  public step1: Basic;
  public step2: Additional;
  public step3: Media;
  public step4: Legal;

  constructor() {
    this.step1 = new Basic();
    this.step2 = new Additional();
    this.step3 = new Media();
    this.step4 = new Legal();
  }

}
