export class Media {
    public logo: string;
    public avatar: string;
    public galery: string[];

    constructor(limitGalery: number) {
        if (!this.galery) {
            this.galery = [];
        }
    }

    addToGalery(base64Picture: string) {
        this.galery.push(base64Picture);
    }
    setGalery(base64Picture: string, i: number) {
        this.galery[i] = base64Picture;
    }
}
