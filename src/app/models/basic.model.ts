export class Basic {
    public tradeName: string;
    public tradeAddress: string;
    public postalCode: string;
    public city: string;
    public phone: string;
    public cellphone: string;
    public fax: string;
    public email: string;
    public website: string;
    public contactName: string;
    public sector: string;
    public subSector: string;
    public description: string;
    public downtown: boolean;
    public PRMAccess: boolean;
}
