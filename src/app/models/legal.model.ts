export class Legal {
    public legalEntity: string;
    public ecbVatNumber: string;
    public representativeName: string;
    public representativePhone: string;
    public representativeEmail: string;
}
