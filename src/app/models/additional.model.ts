export class Additional {
    public typeHour: string;
    public week: Day[];
    public holidayFrom: Date;
    public holidayTo: Date;
    public holidayComment: string;
    public openingSunday: Boolean;
    public CTAButton: string;
    public tradersMessage: string;


    public initWeek(days: string[]) {
        if (!this.week) {
            this.week = [];
            days.forEach(
                (dayString) => {
                    this.week.push(new Day(dayString));
                }
            );
        }

    }

}


export class Period {
    public from: string;
    public to: Date;
}

export class Day {
    public label: string;
    public morning: Period;
    public afternoon: Period;
    public attr: boolean;
    public noon: boolean;

    constructor(label: string) {
        this.label = label;
        this.morning = new Period();
        this.afternoon = new Period();
    }
}
