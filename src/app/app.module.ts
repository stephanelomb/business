import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { HttpModule, Http } from '@angular/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { PersonalPageComponent } from './components/personal-page/personal-page.component';
import { AdministrationComponent } from './components/administration/administration.component';
import { BasicComponent } from './components/administration/basic/basic.component';
import { AdditionalComponent } from './components/administration/additional/additional.component';
import { SubSectorPipe } from './pipes/sub-sector.pipe';
import { MediaComponent } from './components/administration/media/media.component';
import { LegalComponent } from './components/administration/legal/legal.component';
import { StorageService } from './services/storage/storage.service';

export function createTranslateLoader(http: Http) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    PersonalPageComponent,
    AdministrationComponent,
    BasicComponent,
    AdditionalComponent,
    SubSectorPipe,
    MediaComponent,
    LegalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule,
    FormsModule,
    TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: (createTranslateLoader),
                deps: [Http]
            }
        })
  ],
  providers: [StorageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
