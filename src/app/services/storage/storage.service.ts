import { Injectable } from '@angular/core';
import { Administration } from '../../models/administration.model';

@Injectable()
export class StorageService {
  private data: Administration;
  constructor() { }

  public saveAdministration(administration: Administration) {
    this.data = administration;
  }

  public getAdministration(): Administration {
    return this.data;
  }

}
