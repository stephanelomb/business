import { Pipe, PipeTransform } from '@angular/core';
import { SectorList } from '../models/sectorList.model';

@Pipe({
  name: 'subSector',
  pure: false
})
export class SubSectorPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (args) {
      const id: string = args[0];
      return value.filter((item) => {
        return item.idRef === id;
      });
    } else {
      return [];
    }
  }
}
